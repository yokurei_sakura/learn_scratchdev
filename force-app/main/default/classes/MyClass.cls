public with sharing class MyClass {
	public MyClass() {

	}

	public String getAppVersion() {
		return '1.0.0';
	}

	/**
	 * ランダム値を返す
	 * @param floorValue: 返却する最大の値
	 * @return 0〜foorvalueまでのランダムな数値
	 */
	public Integer getRandom(Integer upperLimit) {
		Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, upperLimit);
	}
}
