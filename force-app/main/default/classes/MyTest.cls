@isTest
private class MyTest {

    @isTest
    static void myUnitTest() {
        MyClass demo = new MyClass();
        System.assertEquals(demo.getAppVersion(), '1.0.0');
        // Uncomment the following line to cause a build failure
        // System.assert(false);
    }

    @isTest
    static void myUnitTest2() {
        MyClass demo = new MyClass();

        Test.startTest();
        Integer value = demo.getRandom(5);
        Integer value1 = demo.getRandom(5);
        Integer value2 = demo.getRandom(5);
        Integer value3 = demo.getRandom(5);
        Integer value4 = demo.getRandom(5);
        Integer value5 = demo.getRandom(5);
        Test.stopTest();

        System.assert(value >= 0);
        System.assert(value < 5);
        System.assert(value1 >= 0);
        System.assert(value1 < 5);
        System.assert(value2 >= 0);
        System.assert(value2 < 5);
        System.assert(value3 >= 0);
        System.assert(value3 < 5);
        System.assert(value4 >= 0);
        System.assert(value4 < 5);
        System.assert(value5 >= 0);
        System.assert(value5 < 5);
    }
}
